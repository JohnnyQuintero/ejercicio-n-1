/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author JHONY QUINTERO
 */
public class Sistema {
    //Creando los arreglos
    private String fisica[];
    private String matematicas[];
    private String programacion[];
    //Constructor
    public Sistema() {
    }
    //Metodo Cargar los datos de la vista
    public void cargarCodigo(String info) {
        String dato[] = info.split("\n");
        for (String dato1 : dato) {
            this.setProgramacion(dato[0].split(","));
            this.setFisica(dato[1].split(","));
            this.setMatematicas(dato[2].split(","));
        }
    }
    //Metodo para saber los codigos relacionados con dos materias
    public String[] interseccionAB() {
        String arreglo[] = new String[matematicas.length];
        for (int i = 0; i < arreglo.length; i++) {
            for (int j = i; j < arreglo.length; j++) {
                if (this.getFisica()[j].equals(this.getMatematicas()[j])) {
                    arreglo[j] = this.getFisica()[j];
                }
            }
        }
        return arreglo;
    }
    //Metodo de prueba para probar la salida 
    public String Metodo1() {
        String arr[] = interseccionAB();
        String msg = "";
        for (int i = 0; i < arr.length; i++) {
            msg += arr[i];
        }
        return msg;
    }
    //get y set
    public String[] getFisica() {
        return fisica;
    }

    public void setFisica(String[] fisica) {
        this.fisica = fisica;
    }

    public String[] getMatematicas() {
        return matematicas;
    }

    public void setMatematicas(String[] matematicas) {
        this.matematicas = matematicas;
    }

    public void setProgramacion(String[] programacion) {
        this.programacion = programacion;
    }

    public String[] getProgramacion() {
        return programacion;
    }
    //Main para ir probando los metodos luego si conectar con  la vista
    public static void main(String[] args) {
        Sistema obj = new Sistema();
        obj.cargarCodigo("1,1,1\n1,2,2\n1,3,3");
        System.out.println(obj.Metodo1());
    }
}
